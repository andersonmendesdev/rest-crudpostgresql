// const createTableUser = (connection) =>{
//     return new Promise((resolve, reject) => {
//         connection.query('CREATE TABLE IF NOT EXISTS users (id SERIAL PRIMARY KEY, name VARCHAR(40),email VARCHAR(40), password VARCHAR(40))',(err, result) =>{
//             if(err){
//                 //console.log(err)
//                 reject(err)
//             }else{
//                 resolve(result)
//             }
//         })
//     })    
// }

const addTableUser = (connection, data = {}) =>{
    return new Promise((resolve, reject) => {
        connection.query(`INSERT INTO users (name, email, password) VALUES ('${data.name}', '${data.email}','${data.passwordhash}')`, (err, result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}


const searchAll = (connection) =>{
    return new Promise((resolve, reject) => {
        connection.query('select * from users', (err, result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}

const removeTableUser = (connection, id) =>{
    return new Promise ((resolve, reject) => {
        connection.query(`DELETE FROM users WHERE id = ${id}`, (err, result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}

const getUser = (connection, email) => {
    return new Promise ((resolve, reject) => {
        connection.query(`SELECT * FROM users WHERE email = '${email}'`, (err, result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}

module.exports = {
    addTableUser, searchAll, removeTableUser, getUser
}