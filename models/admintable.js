const alltable = (connection) =>{
    return new Promise((resolve, reject) => {
        connection.query(`SELECT table_name FROM information_schema.tables WHERE table_schema='public'`, (err, result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}

module.exports = {
    alltable
}