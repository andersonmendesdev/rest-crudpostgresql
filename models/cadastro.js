const createTablecadastro = (connection) =>{
    return new Promise((resolve, reject)=>{
        connection.query('CREATE TABLE IF NOT EXISTS cadastro (id SERIAL PRIMARY KEY,nome VARCHAR(40), email VARCHAR(40))',(err, result) =>{
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}