const express = require('express')
const router = express.Router()
const database = require('../database/index')
const ControllersUser = require('../controllers/user')


router.post('/createUser', ControllersUser.InsertUser.bind(null, database))
router.get('/searchUser', ControllersUser.searchUser.bind(null, database))
router.post('/removeuser',ControllersUser.removeUser.bind(null, database))
router.post('/authentication', ControllersUser.authentication.bind(null, database))



module.exports = app => app.use('/', router)