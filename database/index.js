const { Client } = require('pg')
const client = new Client({
    host: 'localhost', 
    port: 5432,
    database: 'crudtest',
    user: 'root',
    password: 'root'
})
client.connect()

client.query('CREATE TABLE IF NOT EXISTS users (iduser SERIAL PRIMARY KEY, name VARCHAR(40), email VARCHAR(40), password VARCHAR(200))')
//client.query('CREATE TABLE IF NOT EXISTS register (idregister SERIAL PRIMARY KEY, name VARCHAR(40), adress VARCHAR(200), city VARCHAR(50), Province CHAR(2))')

module.exports = client



