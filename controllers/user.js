const UserModels = require('../models/user')
const AdminModels = require('../models/admintable')
const {createhash, compareKey} = require('../config/config-bcrypt')

const InsertUser = async(database, req, res) => {
    try{
        const {name, email, password} = req.body
        const passwordhash = await createhash(password)
        const result = await UserModels.addTableUser(database, {
            name,
            email,
            passwordhash
        })
        const data = {
            command: result.command,
            Table :'Users',
            user: req.body
        }        
        res.send({data})

    }catch(e){
        res.status(401).send({error: Object.keys(e.errors)}) 
    }
}
const authentication = async(database, req, res) => {
    try {
        const { email, password} = req.body
        const result = await UserModels.getUser(database, email)
        if(!result.rows[0]){
            return res.send({Error: 'email invalid'})
        }
        const isMatch = await compareKey(password, result.rows[0].password)
        if(isMatch){
            result.rows[0].password = undefined
            const data = {
                command : result.command,
                table :'Users',
                result : result.rows[0]
            }
            return res.send({sucess: true , data})
        }
        res.send({error: 'user invalid'})
        
        
    } catch (e) {
        res.status(401).send({error: Object.keys(e.errors)}) 
    }
}

const searchUser = async( database, req, res) =>{
    try{
        
        const result = await UserModels.searchAll(database)
        const data = {
            command : result.command,
            table :'Users',
            result : result.rows
        }

        res.send({data})

    }catch(e){
        res.status(401).send({error: Object.keys(e.errors)})
    }
}

const removeUser = async(database, req, res) =>{
    try{
        
        const result = await UserModels.removeTableUser(database, req.body.id)
        const data = {
            command: result.command,
            table: 'Users',
            amount: result.rowCount

        }
        
        res.send({data})

    }catch(e){
        res.status(401).send({error: Object.keys(e.errors)})
    }
}

const returnAllTable = async( database, req, res) => {
    try {
        
        const result = await AdminModels.alltable(database)
        const data = {
            command:result.command,
            amount: result.rowCount,
            rows: result.rows
        }

        res.send({data})
        
    } catch (error) {
        res.status(401).send({error: Object.keys(e.errors)})
    }
}



module.exports = {
    InsertUser, searchUser, removeUser, authentication
}