const express = require('express')
const path = require('path')
const app = express()
const bodyParser = require('body-parser')
const http = require('http').Server(app)
const database = require('./database/index')
const port = process.env.PORT || 8081

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false}))


require('./routes/user')(app)

http.listen(port, () => console.log('Running in port '+port))