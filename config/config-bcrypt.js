const bcrypt = require('bcrypt')

const compareKey = async(password, hash_psw) => {
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, hash_psw, (err, isMatch) => {
        if(err){
          reject(err)
        }else{
          resolve(isMatch)
        }
    })  
  })
}
const createhash = async(password) => {
  return new Promise((resolve, reject) => {
      bcrypt.genSalt((err, salt) => {
          if(err){
              reject(err)
          }
            bcrypt.hash(password, salt, (err, hash) => {
              if(err){
                reject(err)
              }else{
                resolve(hash)
              }
            })
    })
  })
}

module.exports = { compareKey, createhash }